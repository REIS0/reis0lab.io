---
title: "Home"
---
Master's student in CESAR School and software developer at CESAR. Chinese and Japanese language enthusiast, sometimes also studying music. Interested in sound and image processing, operational systems, graphics and emulation.

[GitLab](https://gitlab.com/REIS0)\
[GitHub](https://github.com/REIS0)\
[LinkedIn](https://www.linkedin.com/in/aoreis/)\
[Bandcamp](https://reiso.bandcamp.com)
