---
title: "Projects"
draft: false
---

## [Mugi](https://gitlab.com/REIS0/mugi)

A simple library to generate sound data using evolutionary algorithms.

![Alt text](images/tcc-eesynth.png)

## [**AkoFlanger**](https://gitlab.com/REIS0/AkoFlanger)

A flanger LV2 and VST plugin built with Faust and DPF.

![Alt text](images/akoflanger.png)
